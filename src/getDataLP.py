
# Import required modules
from numpy import empty
import pandas
from bs4 import BeautifulSoup
import requests

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36"
}

url = f'https://www.lancaperfume.com.br/buscapagina?fq=C%3a%2f164%2f228%2f&O=OrderByScoreDESC&PS=24&sl=6bd8ca38-bb03-43de-8340-57cbe30b9516&cc=24&sm=0&PageNumber='


data_result = {} # list to populate with the results
url_skus = [] # list with all found url's

productName = []
image = []
fullPrice = []
bestPrice = []
molding = []
composition = []
fabricType = []


#----------------------------------------------------------#
# 1- Pegar todas as url's
for i in range(999):
    print(i)
    
    # Request the page
    page = requests.get(f'{url}{i}', headers=headers)

    # Parsing the page
    html_soup = BeautifulSoup(page.content, 'html.parser')
    
    # Get element
    skus = html_soup.find_all('div', class_ = 'item__data')
    if len(skus) == 0:
        break

    # passing thru the elemnt to get the url
    for sk in skus:
        url_sku = sk.find('a', class_ = 'item__name--text')

        # verify if the element exists
        try:
            # get the link url as add to the list
            url_skus.append(url_sku["href"])
        except AttributeError:
            pass

#----------------------------------------------------------#
# 2- passar por cada url pegando:

for link in url_skus:
    print(link)

    # Request the page
    product_page = requests.get(link, headers=headers)
    # Parsing the page
    product = BeautifulSoup(product_page.content, 'html.parser')


    page_tags = product.find("main", {"id": "main"})   

    try:
        productName_tag = page_tags.find("div", {"class": "productName"})
        productName.append(productName_tag.text)
    except AttributeError:
        productName.append('')

    try:
        image_tag = page_tags.find("a", {"class": "image-zoom"})
        image.append(image_tag["href"])
    except AttributeError:
        image.append('')

    try:
        fullPrice_tag = page_tags.find("strong", {"class": "skuListPrice"})
        fullPrice.append(fullPrice_tag.text)
    except AttributeError:
        fullPrice.append('')

    try:
        bestPrice_tag = page_tags.find("strong", {"class": "skuBestPrice"})
        bestPrice.append(bestPrice_tag.text)
    except AttributeError:
        bestPrice.append('')

    try:
        molding_tag = page_tags.find("td", {"class": "value-field Modelagem"})
        molding.append(molding_tag.text)
    except AttributeError:
        molding.append('')

    try:
        composition_tag = page_tags.find("td", {"class": "value-field Composicao"})
        composition.append(composition_tag.text)
    except AttributeError:
        composition.append('')
    
    try:
        fabricType_tag = page_tags.find("td", {"class": "value-field Tipo-Tecido"})
        fabricType.append(fabricType_tag.text)
    except AttributeError:
        fabricType.append('')

    data_result.update({"Fonte": url_skus, "Image": image, "Names": productName, "Price_Full": fullPrice, "Price_Value": bestPrice, "Modelagem": molding, "Composicao": composition, "Tipo-Tecido": fabricType})
    

    
# 3- salvar dados em csv
data_res = pandas.DataFrame(data=data_result)
data_res.to_csv('data_set_lp.csv', sep=';', index=False)
print("fecho balada")