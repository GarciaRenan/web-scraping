# Import required modules
from time import sleep
import pandas
from lxml import html
import requests
from requests import get
from bs4 import BeautifulSoup
  
# Request the page
url = 'https://www.mariagueixa.com.br/'
page = requests.get(url)

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36"
}
  
# Parsing the page
tree = html.fromstring(page.content)
  
# Get element using XPath
url_link = tree.xpath('//ul/a/@href')

result = {}
sku_list = []

fonte = []
img_product = []
product_name = []
price_full = []
price_value = []
description = []


for idx, ulink in enumerate(url_link):
    sleep(2)
    # print(ulink.split('/'))
    # URL que será buscado os conteúdos
    # if(idx < 5 and idx != 3):
    
    url_search = f"{url}{ulink}?page="
    print("url: ", str(idx), url_search)

    for i in range(9999):
        url_search_final = url_search + str(i+1)
        print("url_search_final: ", url_search_final)

        skus = []
        
        response = get(url_search_final, headers=headers)
        # Espera 5 segundos para acessar cada página
        sleep(3)

        html_soup = BeautifulSoup(response.content, 'html.parser')
        try:
            skus = html_soup.find('div', class_ = 'showcase-item cp-itemprodutos10 js-event-product-click')
            if len(skus) == 0:
                break
            else:
                sku_list.append(skus)
        except TypeError:
            break


        for j in sku_list:
            
            try:
                n = j.find('p', class_ = 'showcase-item-name')
                product_name.append(n.text.strip().title())
            except AttributeError:
                product_name.append('')

            try:
                p_full = j.find("span", class_ = "item-price-max")
                price_full.append(p_full.text)
            except AttributeError:
                price_full.append('')
                
            try:
                p_value = j.find('span', class_ = "item-price-value")
                price_value.append(p_value.text)
            except AttributeError:
                price_value.append('')
                
            try:
                ipr = j.find('div', class_ = "showcase-item-image porn-hover").a.img
                img_product.append(ipr['src'])
            except (AttributeError, TypeError):
                img_product.append('')


            result.update({"Fonte": ulink, "Image": img_product, "Names": product_name, "Price_Full": price_full, "Price_Value": price_value})

        data_res = pandas.DataFrame(data=result)
        data_res.to_csv(f'{ulink.replace("/", "")}.csv', sep=';', index=False)
        print(f'fecho balada: {ulink}')