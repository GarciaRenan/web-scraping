import requests as rq
from bs4 import BeautifulSoup

# Funçãoo para buscar o conteúdo especificado
# -> Tag = tag HTM que está sendo buscada
# -> Identifier = identificador da tag (id, spam, div, etc)
# -> Desc = descrição da classe, spam, etc (ex: class = "titulo-publicacao-widget")

def search(soup, tag, identifier, desc):
    event_found = soup.findAll(tag, {identifier: desc})
    return event_found
