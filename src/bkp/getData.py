# Import required modules
from time import sleep
import pandas
from lxml import html
import requests
from requests import get
from bs4 import BeautifulSoup
  
# Request the page
url = 'https://www.labellamafia.com.br'
page = requests.get(url)
  
# Parsing the page
tree = html.fromstring(page.content)
  
# Get element using XPath
url_link = tree.xpath('//ul/a[@href[starts-with(., "/")]]')

result = {}
sku_list = []

fonte = []
img_product = []
product_name = []
price = []

print(url_link)

for idx, ulink in enumerate(url_link):
    sleep(2)
    # print(ulink.split('/'))
    # URL que será buscado os conteúdos
    # if(idx < 5 and idx != 3):
    url_search = f"{url}{ulink}?page="
    # print("url: ", str(idx) + url_search)



    for i in range(99):
        url_search_final = url_search + str(i+1)
        skus = []
        try:
            response = get(url_search_final)
            # Espera 5 segundos para acessar cada página
            sleep(5)

            html_soup2 = BeautifulSoup(response.text, 'html.parser')
            skus = html_soup2.find_all('div', class_ = 'vtex-search-result-3-x-galleryItem vtex-search-result-3-x-galleryItem--normal vtex-search-result-3-x-galleryItem--small pa4')
        finally:
            print("url_search_final: ", url_search_final)
            print("len: ", len(skus))

        if len(skus) == 0:
            break
        else:
            sku_list.append(skus)

        for i in sku_list:
            for j in i:
                
                n = j.section.find('span', class_ = "labellamafia-product-summary-custom-2-x-productBrand labellamafia-product-summary-custom-2-x-productBrand--qf-shelf labellamafia-product-summary-custom-2-x-brandName labellamafia-product-summary-custom-2-x-brandName--qf-shelf t-body")
                if n.text is not None:
                    product_name.append(n.text)
                    print(n.text)
                else:
                    product_name.append('')


                p = j.section.find('span', class_ = "vtex-product-price-1-x-sellingPriceValue vtex-product-price-1-x-sellingPriceValue--qf-shelf")
                if p.text is not None:
                    price.append(p.text)
                else:
                    price.append('')

                    
                ipr = j.div.find('img', class_ = "labellamafia-product-summary-custom-2-x-imageNormal labellamafia-product-summary-custom-2-x-image labellamafia-product-summary-custom-2-x-image--qf-shelf-product-image labellamafia-product-summary-custom-2-x-mainImageHovered labellamafia-product-summary-custom-2-x-mainImageHovered--qf-shelf-product-image")
                if ipr is not None:
                    img_product.append(ipr['src'])
                else:
                    img_product.append('')

                result.update({"Fonte": ulink, "Image": img_product, "Names": product_name, "Price": price})

        data_frame = ''
        data_frame = pandas.DataFrame(data=result)
        # print(data_frame)
        data_frame.to_csv(f"{ulink.replace('/','')}.csv")