
# Import required modules
import pandas
from bs4 import BeautifulSoup
import requests
from lxml import html

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36"
}

data_result = []
url = 'https://www.lancaperfume.com.br/buscapagina?fq=C%3a%2f164%2f228%2f&O=OrderByScoreDESC&PS=24&sl=6bd8ca38-bb03-43de-8340-57cbe30b9516&cc=24&sm=0&PageNumber='

for i in range(99):
    # Request the page
    print(i)
    page = requests.get(f'{url}{i}', headers=headers)

    # Parsing the page
    html_soup = BeautifulSoup(page.content, 'html.parser')
    
    # Get element using XPath
    skus = html_soup.find('div', class_ = 'item__main')

    if skus == None:
        print('fim')
        break

    print(skus)
    name = skus.find_all('a', class_ = 'item__name--text')
    price = skus.find_all('span', class_ = 'price__bestPrice--value')

    data_result.append({"Nome": name[0].text, "Preço": price[0].text})

# data_frame = pandas.DataFrame(data=prices)

print(data_result)

# data_res = pandas.DataFrame(data=prices)
# data_res.to_csv('result.csv', index=False)