# Import required modules
import pandas
from lxml import html
import requests
  
# Request the page
page = requests.get('https://www.labellamafia.com.br')
  
# Parsing the page
tree = html.fromstring(page.content)
  
# Get element using XPath
prices = tree.xpath('//@href[starts-with(., "/")]')

data_frame = pandas.DataFrame(data=prices)

print(data_frame)