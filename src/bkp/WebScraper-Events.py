import pandas
from GetSoup import get_soup
from SearchEvent import search
import csv


# Função para retornar os títulos
def format_text(element):
    text = element.text.title()
    return text

# Criação um dicionário vazio para adicioanr os eventos futuramente e listas para adicionar os eventos títulos e datas
events = {}
titles = {}
dates = {}


# URL que será buscado os conteúdos
url = "http://www.labellamafia.com.br/mulher/blusas"

# Busca o soup
soup = get_soup(url)

# Extração dos dados desejados
# found_titles = search(soup, "div", "id", "gallery-layout-container")
found_dates = search(soup, "spam", "class", "pr0 items-stretch vtex-flex-layout-0-x-stretchChildrenWidth   flex")

# Salvar os dados buscados em um dicionário
for i in range(0, len(found_dates)-1):
    # titles.insert(i, format_text(found_titles[i]))
    dates.insert(i, format_text(found_dates[i]))

events.update({"Names": titles, "Date": dates})

# Usando a biblioteca Pandas para estruturar os dados
data_frame = pandas.DataFrame(data=events)

print("\nEVENTOS {}:\n{}".format(url, data_frame))